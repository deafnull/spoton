﻿namespace SpotOn.App.Models
{
    public class SuggestMusicViewModel
    {
        public string FavouriteArtistIds { get; set; }
        public string FavouriteTrackIds { get; set; }
        public string FavouriteGenres { get; set; }
    }
}
