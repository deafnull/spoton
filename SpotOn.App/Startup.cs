﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SpotOn.Core;
using Swashbuckle.AspNetCore.Swagger;

namespace SpotOn.App
{
    public class Startup
    {
        private const string ClientId = "e119283bc5b24d9fbb0f57403763a463";
        private const string SecretKey = "d82db0c8b4cb4ea79235d9bdad2ad894";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("doc", new Info { Title = "SpotOn API" });
            });

            services.AddTransient<ISpotifyClient, SpotifyClient>();
            services.AddTransient((s) => new Credentials(ClientId, SecretKey));
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/SuggestMusic/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=SuggestMusic}/{action=Index}");
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "swag";
                c.SwaggerEndpoint("../swagger/doc/swagger.json", "SpotOn API");
            });
        }
    }
}
