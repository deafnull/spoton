﻿using Microsoft.AspNetCore.Mvc;
using SpotOn.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpotOn.App.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SpotifyController : ControllerBase
    {
        private readonly ISpotifyClient spotify;

        public SpotifyController(ISpotifyClient spotify)
        {
            this.spotify = spotify;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Artist>>> FindArtists(string input)
        {
            List<Artist> result = new List<Artist>();
            if (!string.IsNullOrWhiteSpace(input))
            {
                await spotify.Authorize();
                IEnumerable<Artist> artists = await spotify.FindArtists(input);
                result.AddRange(artists);
            }
            return result;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Track>>> FindTracks(string input)
        {
            List<Track> result = new List<Track>();
            if (!string.IsNullOrWhiteSpace(input))
            {
                await spotify.Authorize();
                IEnumerable<Track> tracks = await spotify.FindTracks(input);
                result.AddRange(tracks);
            }
            return result;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Genre>>> GetGenres()
        {
            await spotify.Authorize();
            return (await spotify.GetGenres()).ToList();
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Track>>> GetRecommendationsForNames(string artistName = null, string trackName = null, string genreName = null)
        {
            List<Track> result = new List<Track>();
            if (!string.IsNullOrWhiteSpace(artistName) || !string.IsNullOrWhiteSpace(trackName) || !string.IsNullOrWhiteSpace(genreName))
            {
                await spotify.Authorize();

                string artistId = null;
                if (!string.IsNullOrWhiteSpace(artistName))
                {
                    IEnumerable<Artist> inputArtists = await spotify.FindArtists(artistName);
                    artistId = inputArtists.FirstOrDefault()?.Id;
                }

                string trackId = null;
                if (!string.IsNullOrWhiteSpace(trackName))
                {
                    IEnumerable<Track> inputTracks = await spotify.FindTracks(trackName);
                    trackId = inputTracks.FirstOrDefault()?.Id;
                }

                string genreId = null;
                if (!string.IsNullOrWhiteSpace(genreName))
                {
                    IEnumerable<Genre> inputGenres = await spotify.FindGenres(genreName);
                    genreId = inputGenres.FirstOrDefault()?.Name;
                }

                if (artistId != null || trackId != null || genreId != null)
                {
                    IEnumerable<Track> tracks = await spotify.GetRecommendations(new[] { artistId }, new[] { trackId }, new[] { genreId });
                    result.AddRange(tracks);
                }
            }
            return result;
        }
    }
}