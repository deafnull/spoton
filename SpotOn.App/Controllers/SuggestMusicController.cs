﻿using Microsoft.AspNetCore.Mvc;
using SpotOn.App.Models;
using SpotOn.Core;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SpotOn.App.Controllers
{
    public class SuggestMusicController : Controller
    {
        private readonly ISpotifyClient spotify;

        public SuggestMusicController(ISpotifyClient spotifyClient)
        {
            this.spotify = spotifyClient;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Results(SuggestMusicViewModel model)
        {
            List<Track> recommendations = new List<Track>();
            string delimiter = ",";

            if (string.IsNullOrWhiteSpace(model.FavouriteArtistIds?.Replace(delimiter, string.Empty))
                && string.IsNullOrWhiteSpace(model.FavouriteTrackIds?.Replace(delimiter, string.Empty))
                && string.IsNullOrWhiteSpace(model.FavouriteGenres?.Replace(delimiter, string.Empty)))
            {
                ModelState.AddModelError("Model", "At least one question must be answered.");
            }

            string[] artistIds = model.FavouriteArtistIds?.Split(delimiter);
            string[] trackIds = model.FavouriteTrackIds?.Split(delimiter);
            string[] genreIds = model.FavouriteGenres?.Split(delimiter);

            if ((artistIds?.Count() ?? 0) + (trackIds?.Count() ?? 0) + (genreIds?.Count() ?? 0) > 5)
            {
                ModelState.AddModelError("Model", "Only 5 artist, track or genre items are allowed.");
            }
            else
            {
                await spotify.Authorize();
                IEnumerable<Track> tracks = await spotify.GetRecommendations(artistIds, trackIds, genreIds);
                if (tracks.Any())
                {
                    recommendations.AddRange(tracks);
                }
                else
                {
                    ModelState.AddModelError("Model", "No recommendations found.");
                }
            }

            if (!ModelState.IsValid)
            {
                return View(nameof(Index), model);
            }

            return View(recommendations);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}