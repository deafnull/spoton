using SpotOn.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SpotOn.Test
{
    public class SpotifyTest : IClassFixture<SpotifyFixture>
    {
        private readonly SpotifyClient client;

        public SpotifyTest(SpotifyFixture data)
        {
            client = data.Client;
        }

        [Theory]
        [InlineData("grateful", "Grateful Dead")]
        [InlineData("fLoYd", "Pink Floyd")]
        public async Task FindArtist(string input, string expected)
        {
            IEnumerable<Artist> artists = await client.FindArtists(input);

            Assert.Contains(artists, a => a.Name == expected);
        }

        [Fact]
        public void FindArtistThrowExceptionIfNoInput()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => { await client.FindArtists(null); } );
        }

        [Theory]
        [InlineData("Paul McCartney", "The Beatles")]
        [InlineData("Cult of Luna", "Neurosis")]
        public async Task FindRelatedArtists(string input, string expectedRelatedArtist)
        {
            IEnumerable<Artist> inputArtists = await client.FindArtists(input);
            string artistId = inputArtists.FirstOrDefault()?.Id;

            IEnumerable<Artist> artists = await client.FindRelatedArtists(artistId);

            Assert.Contains(artists, a => a.Name == expectedRelatedArtist);
        }

        [Fact]
        public void FindRelatedArtistsThrowExceptionIfNoInput()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => { await client.FindRelatedArtists(null); });
        }

        [Theory]
        [InlineData("Yellow Submarine", "Yellow Submarine", "The Beatles")]
        public async Task FindTracks(string input, string expectedTrack, string expectedArtist)
        {
            IEnumerable<Track> tracks = await client.FindTracks(input);

            Assert.Contains(tracks, a => a.Name.Contains(expectedTrack));
            Assert.Contains(tracks, a => a.DisplayName == $"{a.Name} by {expectedArtist}");
        }

        [Fact]
        public void FindTracksThrowExceptionIfNoInput()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => { await client.FindTracks(null); });
        }

        [Fact]
        public async Task GetGenres()
        {
            IEnumerable<Genre> genres = await client.GetGenres();

            Assert.True(genres.Any());
        }

        [Theory]
        [InlineData("rOck", "rock")]
        public async Task FindGenres(string input, string expected)
        {
            IEnumerable<Genre> genres = await client.FindGenres(input);

            Assert.Contains(genres, a => a.Name == expected);
        }

        [Theory, CombinatorialData]
        public async Task GetRecommendations([CombinatorialValues(null, "The Beatles")]string inputArtist, [CombinatorialValues(null, "Toxic")] string inputTrack, [CombinatorialValues(null, "death-metal")] string inputGenre)
        {
            IEnumerable<Artist> artists = inputArtist == null ? new List<Artist>() : await client.FindArtists(inputArtist);
            string artistId = artists.FirstOrDefault()?.Id;

            IEnumerable<Track> tracks = inputTrack == null ? new List<Track>() : await client.FindTracks(inputTrack);
            string trackId = tracks.FirstOrDefault()?.Id;

            IEnumerable<Genre> genres = inputGenre == null ? new List<Genre>() : await client.FindGenres(inputGenre);
            string genreName = genres.FirstOrDefault()?.Name;

            if (inputArtist != null || inputTrack != null || inputGenre != null)
            {
                IEnumerable<Track> result = await client.GetRecommendations(new[] { artistId }, new[] { trackId }, new[] { genreName });
                Assert.True(result.Any());
            }
        }
    }
}
