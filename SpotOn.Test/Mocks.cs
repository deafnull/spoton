﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Spotify.API.NetCore.Models;
using SpotOn.Core;

namespace SpotOn.Test
{
    public class MSpotifyClient : ISpotifyClient
    {
        public Task Authorize()
        {
            return Task.Delay(0);
        }

        public async Task<IEnumerable<Artist>> FindArtists(string input)
        {
            await Task.Delay(0);
            return new[] { new Artist(new SimpleArtist() { Id = Guid.NewGuid().ToString(), Name = input }) { } };
        }

        public async Task<IEnumerable<Genre>> FindGenres(string genreName)
        {
            await Task.Delay(0);
            return new[] { new Genre(genreName) };
        }

        public async Task<IEnumerable<Artist>> FindRelatedArtists(string artistId)
        {
            await Task.Delay(0);
            return new[] { new Artist(new SimpleArtist() { Id = Guid.NewGuid().ToString(), Name = Guid.NewGuid().ToString() }) { } };
        }

        public async Task<IEnumerable<Track>> FindTracks(string input)
        {
            await Task.Delay(0);
            return new[] { new Track(new SimpleTrack() { Id = Guid.NewGuid().ToString(), Name = input, Artists = new List<SimpleArtist> { new SimpleArtist() { Id = Guid.NewGuid().ToString(), Name = Guid.NewGuid().ToString() } } }) };
        }

        public async Task<IEnumerable<Genre>> GetGenres()
        {
            await Task.Delay(0);
            return new[] { new Genre(Guid.NewGuid().ToString()), new Genre(Guid.NewGuid().ToString()) };
        }

        public virtual async Task<IEnumerable<Track>> GetRecommendations(string[] artistIds, string[] trackIds, string[] genreIds)
        {
            await Task.Delay(0);
            return new[] { new Track(new SimpleTrack() { Id = Guid.NewGuid().ToString(), Name = Guid.NewGuid().ToString(), Artists = new List<SimpleArtist> { new SimpleArtist() { Id = Guid.NewGuid().ToString(), Name = Guid.NewGuid().ToString() } } }) };
        }
    }

    public class MNoRecommendationsSpotifyClient : MSpotifyClient
    {
        public override async Task<IEnumerable<Track>> GetRecommendations(string[] artistIds, string[] trackIds, string[] genreIds)
        {
            await Task.Delay(0);
            return new List<Track>();
        }
    }
}
