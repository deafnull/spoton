﻿using Microsoft.AspNetCore.Mvc;
using SpotOn.App.Controllers;
using SpotOn.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SpotOn.Test
{
    public class SpotifyControllerTest
    {
        private readonly SpotifyController controller = new SpotifyController(new MSpotifyClient());

        [Fact]
        public async Task FindArtists()
        {
            string input = "test";

            ActionResult<IEnumerable<Artist>> result = await controller.FindArtists(input);

            Assert.NotNull(result.Value);
            Assert.True(result.Value.Any());
            Assert.Contains(result.Value, x => x.Name == input);
        }

        [Fact]
        public async Task FindArtistsNoInput()
        {
            string input = null;

            ActionResult<IEnumerable<Artist>> result = await controller.FindArtists(input);

            Assert.NotNull(result.Value);
            Assert.False(result.Value.Any());
        }

        [Fact]
        public async Task FindTracks()
        {
            string input = "test";

            ActionResult<IEnumerable<Track>> result = await controller.FindTracks(input);

            Assert.NotNull(result.Value);
            Assert.True(result.Value.Any());
            Assert.Contains(result.Value, x => x.Name == input);
        }

        [Fact]
        public async Task FindTracksNoInput()
        {
            string input = null;

            ActionResult<IEnumerable<Track>> result = await controller.FindTracks(input);

            Assert.NotNull(result.Value);
            Assert.False(result.Value.Any());
        }

        [Fact]
        public async Task GetGenres()
        {
            ActionResult<IEnumerable<Genre>> result = await controller.GetGenres();

            Assert.NotNull(result.Value);
            Assert.True(result.Value.Any());
        }

        [Fact]
        public async Task GetRecommendationsForArtist()
        {
            string input = "test";

            ActionResult<IEnumerable<Track>> result = await controller.GetRecommendationsForNames(artistName: input);

            Assert.NotNull(result.Value);
            Assert.True(result.Value.Any());
        }

        [Fact]
        public async Task GetRecommendationsForTrack()
        {
            string input = "test";

            ActionResult<IEnumerable<Track>> result = await controller.GetRecommendationsForNames(trackName: input);

            Assert.NotNull(result.Value);
            Assert.True(result.Value.Any());
        }

        [Fact]
        public async Task GetRecommendationsForGenre()
        {
            string input = "test";

            ActionResult<IEnumerable<Track>> result = await controller.GetRecommendationsForNames(genreName: input);

            Assert.NotNull(result.Value);
            Assert.True(result.Value.Any());
        }

        [Fact]
        public async Task GetRecommendationsForNamesNoInput()
        {
            string input = null;

            ActionResult<IEnumerable<Track>> result = await controller.GetRecommendationsForNames(input, input, input);

            Assert.NotNull(result.Value);
            Assert.False(result.Value.Any());
        }
    }
}
