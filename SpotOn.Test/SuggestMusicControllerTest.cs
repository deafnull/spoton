﻿using Microsoft.AspNetCore.Mvc;
using SpotOn.App.Controllers;
using SpotOn.App.Models;
using SpotOn.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SpotOn.Test
{
    public class SuggestMusicControllerTest
    {
        private readonly SuggestMusicController controller = new SuggestMusicController(new MSpotifyClient());

        [Theory, CombinatorialData]
        public async Task Results([CombinatorialValues(null, "A", "A,B")]string artists,
            [CombinatorialValues(null, "T", "T,U")]string tracks,
            [CombinatorialValues(null, "G", "G,H")]string genres)
        {
            SuggestMusicViewModel inputModel = new SuggestMusicViewModel()
            {
                FavouriteArtistIds = artists,
                FavouriteTrackIds = tracks,
                FavouriteGenres = genres
            };

            IActionResult result = await controller.Results(inputModel);

            if (artists != null || tracks != null || genres != null)
            {
                if (CountItems(inputModel) <= 5)
                {
                    AssertResultsSuccess(result);
                }
                else
                {
                    AssertResultsFailure(result);
                }
            }
            else
            {
                AssertResultsFailure(result);
            }
        }

        private int CountItems(SuggestMusicViewModel model)
        {
            return (model.FavouriteArtistIds?.Split(",").Length ?? 0) + (model.FavouriteTrackIds?.Split(",").Length ?? 0) + (model.FavouriteGenres?.Split(",").Length ?? 0);
        }

        private static void AssertResultsSuccess(IActionResult result)
        {
            ViewResult viewResult = Assert.IsType<ViewResult>(result);
            IEnumerable<Track> resultModel = Assert.IsAssignableFrom<IEnumerable<Track>>(
                viewResult.ViewData.Model);
            Assert.True(resultModel.Any());
        }

        private static void AssertResultsFailure(IActionResult result)
        {
            ViewResult badRequestResult = Assert.IsType<ViewResult>(result);
            Assert.IsAssignableFrom<SuggestMusicViewModel>(badRequestResult.ViewData.Model);
            Assert.False(badRequestResult.ViewData.ModelState.IsValid);
            Assert.Equal(1, badRequestResult.ViewData.ModelState.ErrorCount);
        }

        [Fact]
        public void Index()
        {
            IActionResult result = controller.Index();

            ViewResult viewResult = Assert.IsAssignableFrom<ViewResult>(result);
            Assert.True(viewResult.ViewData.ModelState.IsValid);
        }

        [Fact]
        public async Task ResultsSuccess()
        {
            SuggestMusicViewModel inputModel = new SuggestMusicViewModel()
            {
                FavouriteArtistIds = "test",
                FavouriteTrackIds = "test,test",
                FavouriteGenres = null
            };

            IActionResult result = await controller.Results(inputModel);

            AssertResultsSuccess(result);
        }

        [Fact]
        public async Task ResultsFailureNoInput()
        {
            SuggestMusicViewModel inputModel = new SuggestMusicViewModel()
            {
                FavouriteArtistIds = null,
                FavouriteTrackIds = null,
                FavouriteGenres = null
            };

            IActionResult result = await controller.Results(inputModel);

            AssertResultsFailure(result);
        }

        [Fact]
        public async Task ResultsFailureTooManyItems()
        {
            SuggestMusicViewModel inputModel = new SuggestMusicViewModel()
            {
                FavouriteArtistIds = "t,t,t,t,t,t",
            };

            IActionResult result = await controller.Results(inputModel);

            AssertResultsFailure(result);
        }

        [Fact]
        public async Task ResultsFailureNoRecommendationsFound()
        {
            SuggestMusicController controller = new SuggestMusicController(new MNoRecommendationsSpotifyClient());
            SuggestMusicViewModel inputModel = new SuggestMusicViewModel()
            {
                FavouriteArtistIds = "t",
            };

            IActionResult result = await controller.Results(inputModel);

            AssertResultsFailure(result);
        }
    }
}
