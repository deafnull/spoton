﻿using SpotOn.Core;
using System;
using System.Threading.Tasks;

namespace SpotOn.Test
{
    public class SpotifyFixture : IDisposable
    {
        private const string ClientId = "e119283bc5b24d9fbb0f57403763a463";
        private const string SecretKey = "d82db0c8b4cb4ea79235d9bdad2ad894";

        public SpotifyFixture()
        {
            Client = new SpotifyClient(new Credentials(ClientId, SecretKey));
            Task t = Client.Authorize();
            t.Wait();
        }

        public SpotifyClient Client { get; }

        public void Dispose()
        {
            Client?.Dispose();
        }
    }
}
