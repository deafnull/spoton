﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpotOn.Core
{
    public interface ISpotifyClient
    {
        Task Authorize();
        Task<IEnumerable<Artist>> FindArtists(string input);
        Task<IEnumerable<Genre>> FindGenres(string genreName);
        Task<IEnumerable<Artist>> FindRelatedArtists(string artistId);
        Task<IEnumerable<Track>> FindTracks(string input);
        Task<IEnumerable<Genre>> GetGenres();
        Task<IEnumerable<Track>> GetRecommendations(string[] artistIds, string[] trackIds, string[] genreIds);
    }
}