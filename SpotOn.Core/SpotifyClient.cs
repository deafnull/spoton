﻿using Spotify.API.NetCore;
using Spotify.API.NetCore.Auth;
using Spotify.API.NetCore.Enums;
using Spotify.API.NetCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpotOn.Core
{
    public class SpotifyClient : ISpotifyClient, IDisposable
    {
        private readonly SpotifyWebAPI spotify = new SpotifyWebAPI();
        private readonly Credentials credentials;

        public SpotifyClient(Credentials credentials)
        {
            this.credentials = credentials;
        }

        public async Task Authorize()
        {
            if (spotify.AccessToken == null)
            {
                ClientCredentialsAuth auth = new ClientCredentialsAuth()
                {
                    ClientId = credentials.ClientId,
                    ClientSecret = credentials.SecretKey,
                    Scope = Scope.None
                };
                Token token = await auth.DoAuthAsync();
                spotify.TokenType = token.TokenType;
                spotify.AccessToken = token.AccessToken;
                spotify.UseAuth = true;
            }
        }

        public async Task<IEnumerable<Artist>> FindArtists(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            SearchItem response = await spotify.SearchItemsAsync(input, SearchType.Artist);

            HandleError(response);

            return response?.Artists?.Items?.Select(a => new Artist(a)) ?? new List<Artist>();
        }

        private static void HandleError(BasicModel response)
        {
            if (response.HasError())
                throw new Exception($"Message: {response.Error.Message} Status: {response.Error?.Status}");
        }

        public async Task<IEnumerable<Track>> FindTracks(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            SearchItem response = await spotify.SearchItemsAsync(input, SearchType.Track);

            HandleError(response);

            return response?.Tracks?.Items.Select(t => new Track(t)) ?? new List<Track>();
        }

        public async Task<IEnumerable<Artist>> FindRelatedArtists(string artistId)
        {
            if (artistId == null)
                throw new ArgumentNullException(nameof(artistId));

            SeveralArtists response = await spotify.GetRelatedArtistsAsync(artistId);

            HandleError(response);

            return response?.Artists?.Select(a => new Artist(a)) ?? new List<Artist>();
        }

        public async Task<IEnumerable<Track>> GetRecommendations(string[] artistIds, string[] trackIds, string[] genreIds)
        {
            List<Track> result = new List<Track>();

            if (artistIds?.Any() != null || trackIds?.Any() != null || genreIds?.Any() != null)
            {
                Recommendations response = await spotify.GetRecommendationsAsync(artistSeed: artistIds?.ToList(), trackSeed: trackIds?.ToList(), genreSeed: genreIds?.ToList());
                HandleError(response);
                result.AddRange(response.Tracks.Select(t => new Track(t)));
            }

            return result;
        }

        public async Task<IEnumerable<Genre>> FindGenres(string genreName)
        {
            if (genreName == null)
                throw new ArgumentNullException(nameof(genreName));

            IEnumerable<Genre> allGenres = await GetGenres();
            return allGenres.Where(g => g.Name.Contains(genreName, StringComparison.InvariantCultureIgnoreCase));
        }

        public async Task<IEnumerable<Genre>> GetGenres()
        {
            RecommendationSeedGenres response = await spotify.GetRecommendationSeedsGenresAsync();

            HandleError(response);

            return response?.Genres.Select(g => new Genre(g)) ?? new List<Genre>();
        }

        public void Dispose()
        {
            spotify?.Dispose();
        }
    }
}
