﻿namespace SpotOn.Core
{
    public class Credentials
    {
        public Credentials(string clientId, string secretKey)
        {
            ClientId = clientId;
            SecretKey = secretKey;
        }

        public string ClientId { get; }
        public string SecretKey { get; }
    }
}
