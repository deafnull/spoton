﻿using Spotify.API.NetCore.Models;
using System.Collections.Generic;
using System.Linq;

namespace SpotOn.Core
{
    public class Track
    {
        private readonly SimpleTrack simpleTrack;
        private readonly FullTrack fullTrack;
        private readonly List<Artist> artists = new List<Artist>();

        public Track(SimpleTrack simpleTrack)
        {
            this.simpleTrack = simpleTrack;
            if (simpleTrack?.Artists != null)
                foreach (SimpleArtist a in simpleTrack.Artists)
                {
                    artists.Add(new Artist(a));
                }
        }

        public Track(FullTrack fullTrack)
        {
            this.fullTrack = fullTrack;
            if (fullTrack?.Artists != null)
                foreach (SimpleArtist a in fullTrack.Artists)
                {
                    artists.Add(new Artist(a));
                }
        }

        public string Id => fullTrack?.Id ?? simpleTrack?.Id;
        public string Name => fullTrack?.Name ?? simpleTrack?.Name;
        public string Url => fullTrack?.PreviewUrl ?? simpleTrack?.PreviewUrl;
        public string ArtistName => string.Join(" and ", artists.Select(a => a.Name));
        public string DisplayName
        {
            get
            {
                return $"{Name} by {ArtistName}";
            }
        }
    }
}
