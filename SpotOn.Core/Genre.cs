﻿namespace SpotOn.Core
{
    public class Genre
    {
        public Genre(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
