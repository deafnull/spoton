﻿using Spotify.API.NetCore.Models;

namespace SpotOn.Core
{
    public class Artist
    {
        private readonly FullArtist fullArtist;
        private readonly SimpleArtist simpleArtist;

        public Artist(FullArtist fullArtist)
        {
            this.fullArtist = fullArtist;
        }

        public Artist(SimpleArtist simpleArtist)
        {
            this.simpleArtist = simpleArtist;
        }

        public string Id => fullArtist?.Id ?? simpleArtist?.Id;
        public string Name => fullArtist?.Name ?? simpleArtist?.Name;
    }
}
